# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .address import Address


def register():
    Pool.register(
        Address,
        module='party_address_alias', type_='model')
