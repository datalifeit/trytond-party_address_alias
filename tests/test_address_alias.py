# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class TestCase(ModuleTestCase):
    """Test module"""
    module = 'party_address_alias'

    @with_transaction()
    def test_party(self):
        """Create party"""
        Party = Pool().get('party.party')
        party1, = Party.create([{
                    'name': 'Party 1',
                    }])
        self.assert_(party1.id)

    @with_transaction()
    def test_address(self):
        """Create address"""
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')

        party1, = Party.create([{
                    'name': 'Party 1',
                    }])

        address, = Address.create([{
                    'party': party1.id,
                    'street': 'St sample, 15',
                    'city': 'City',
                    'alias': 'PS'
                    }])
        self.assert_(address.id)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCase))
    return suite
